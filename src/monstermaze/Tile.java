/*
 * @(#)Tile.java	1.0 19/12/18 Jan Feific
 * 
 * Copyright (c) 2019 Jan Feific. All Rights Reserved.
 */

package monstermaze;

/**
 * Enum representing content of a maze tile.
 * It can be either wall or empty (passage).
 */
public enum Tile {
    EMPTY, WALL;

    /**
     * Convert tile to a character for console output.
     */
    @Override
    public final String toString() {
	if (this == Tile.EMPTY) {
	    return ".";
	} else {
	    return "#";
	}
    }
}
