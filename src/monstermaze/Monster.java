package monstermaze;

import java.util.Random;

public final class Monster {
    private Position position = null;
    private Maze maze = null;
    private Direction moveSet[] = new Direction[4];
    private long lastMove = 0;
    private Random r = new Random();
    
    public Monster(Maze m) {
	maze = m;
	moveSet[0] = Direction.WEST;
	moveSet[1] = Direction.EAST;
	moveSet[2] = Direction.SOUTH;
	moveSet[3] = Direction.NORTH;

	// ensure that the monster is created at an empty tile
	do {
	    int spawnCoordinateX = r.nextInt(maze.getMazeWidth());
	    int spawnCoordinateY = maze.getMazeHeight() - r.nextInt(5) - 1;
	    position = new Position(spawnCoordinateX, spawnCoordinateY, Direction.NORTH);
	} while (maze.getTileAtPosition(position).equals(Tile.WALL));
	System.out.format("T Rex spawned at x: %d, y: %d.\n", position.getX(), position.getY());
    }

    public final Position getPosition() {
	return position;
    }
    
    private final boolean hasLineOfSight(Position otherPosition) {
	int thisX = position.getX();
	int otherX = otherPosition.getX();
	int thisY = position.getY();
	int otherY = otherPosition.getY();
	if (thisX == otherX) {
	    for (int i = Math.min(thisY, otherY); i < Math.max(thisY, otherY); ++i) {
		if (maze.getTileAtPosition(new Position(thisX, i, Direction.NORTH)).equals(Tile.WALL)) {
		    return false;
		}
	    }
	    return true;
	}
	if (thisY == otherY) {
	    for (int i = Math.min(thisX, otherX); i < Math.max(thisX, otherX); ++i) {
		if (maze.getTileAtPosition(new Position(i, thisY, Direction.NORTH)).equals(Tile.WALL)) {
		    return false;
		}
	    }
	    return true;
	}
	return false;
    }
    
    public final void moveTo(Position targetPosition, long thisMove) {
	if (hasLineOfSight(targetPosition)) {
	    // Do special move
	    System.out.println("The monster has line of sight.");
	}

	// With no line of sight, use the basic pathfinding
	if ((thisMove - lastMove) > 1000000000L) {
	    if (r.nextInt(2) == 0) {
		if (targetPosition.getX() > position.getX()) {
		    position.setDirection(Direction.EAST);
		} else {
		    position.setDirection(Direction.WEST);
		}
	    } else {
		if (targetPosition.getY() > position.getY()) {
		    position.setDirection(Direction.SOUTH);
		} else {
		    position.setDirection(Direction.NORTH);
		}
	    }
	    if (maze.getTileAtPosition(position.getRelativePosition(1)).equals(Tile.EMPTY)) {
		position.advance();
	    } // else lies in wait
	    
	    System.out.format("T Rex moves to x: %d, y: %d.\n", position.getX(), position.getY());
	    lastMove = thisMove;
	}
    }
    
}
