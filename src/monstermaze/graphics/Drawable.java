/*
 * @(#)Drawable.java	1.0 19/12/18 Jan Feific
 * 
 * Copyright (c) 2019 Jan Feific. All Rights Reserved.
 */

package monstermaze.graphics;

/**
 * Interface which represents objects that can be drawn onto a canvas.
 */
interface Drawable {
    public void draw();
}
