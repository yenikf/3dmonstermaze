package monstermaze.graphics;

import java.io.FileInputStream;
import java.io.IOException;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import monstermaze.Maze;
import monstermaze.Monster;
import monstermaze.Player;

public final class AppFrame extends Application {
    // graphics parameters
    private static final int FRAME_WIDTH = 512;
    private static final int FRAME_HEIGHT = 384;
    private static final int VIEW_WIDTH = 400;
    private static final int VIEW_HEIGHT = 384;
    private static final int TEXT_POS_X = 432;
    private static final int TEXT_POS_Y = 180;
    // graphics objects
    private Canvas viewCanvas = null;
    private View view = null;
    private Text scoreView = null;
    // gameplay objects
    private Maze maze = null;
    private Player player = null;
    private Monster tRex = null;
    
    @Override
    public void init() {
	maze = new Maze();
	player = new Player(maze);
	tRex = new Monster(maze);
	System.out.println(maze);
    }
    
    @Override
    public void start(Stage stage) {
	viewCanvas = new Canvas(VIEW_WIDTH, VIEW_HEIGHT);
	view = new View(viewCanvas);
	scoreView = new Text();
	scoreView.setFont(loadFont());
	scoreView.setX(TEXT_POS_X);
	scoreView.setY(TEXT_POS_Y);
	scoreView.setText("SCORE\n 0");
	Group root = new Group(viewCanvas, scoreView);
	
	Scene scene = new Scene(root, FRAME_WIDTH, FRAME_HEIGHT, Color.LIGHTGRAY);
	stage.setTitle("3D Monster Maze");
	stage.setResizable(false);
	stage.setScene(scene);
	scene.setOnKeyPressed(player.handleInput);
	
	AnimationTimer gameCycle = new AnimationTimer() {
	    boolean endGame = false;
	    @Override
	    public void handle(long currentTime) {
		if (endGame == true) {
		    this.stop();
		}

		// handle player-monster collision
		if (tRex.getPosition().equals(player.getPosition())){
		    view.drawScreenFailure();
		    endGame = true;
		    return;
		}
		
		// handle player-exit collision
		if (maze.getExitPosition().equals(player.getPosition())) {
		    view.drawScreenSuccess();
		    endGame = true;
		    return;
		}
		tRex.moveTo(player.getPosition(), currentTime);
		view.draw(maze, player.getPosition());
		scoreView.setText(String.format("SCORE\n %3d", player.getScore()));
	    }
	};
	gameCycle.start();
	stage.show();
    }

    public void runApplication() {
	launch();
    }
    
    // https://www.dafont.com/zx81.font
    private Font loadFont() {
	Font font = null;
	try (FileInputStream is = new FileInputStream("./font/zx81.ttf")) {
	    font = Font.loadFont(is, 16);
	} catch(IOException e) {
	    e.printStackTrace();
	}
	return font;
    }
}
