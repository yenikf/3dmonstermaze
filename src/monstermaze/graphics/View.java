package monstermaze.graphics;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javafx.scene.canvas.Canvas;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

import monstermaze.Maze;
import monstermaze.Position;
import monstermaze.Side;
import monstermaze.Tile;

final class View {
    private static final int DRAW_DISTANCE = 6;
    private static final int ELEMENT_SIZE = 16;
    private Canvas canvas = null;
    private Image screenFailure = null;
//    private Image screenSuccess = null;
    private Image viewElements[] = new Image[21];
    private Image tRex[] = new Image[10];
    private Drawable drawExit[] = new Drawable[DRAW_DISTANCE];
    private Drawable drawFrontWall[] = new Drawable[DRAW_DISTANCE];
    private Drawable drawRightWall[] = new Drawable[DRAW_DISTANCE];
    private Drawable drawRightPassage[] = new Drawable[DRAW_DISTANCE];
    private Drawable drawLeftWall[] = new Drawable[DRAW_DISTANCE];
    private Drawable drawLeftPassage[] = new Drawable[DRAW_DISTANCE];
        
    public View(Canvas canv) {
	canvas = canv;
	initDrawRoutines();
	try (FileInputStream is = new FileInputStream("./images/Failure.png")) {
	    screenFailure = new Image(is);
	} catch (IOException e) {
	    e.printStackTrace();
	}
	for (int i = 0; i < 10; ++i) {
	    try (FileInputStream is = new FileInputStream("./images/Rex" + i + ".png")) {
		tRex[i] = new Image(is);
	    } catch (IOException e) {
		e.printStackTrace();
	    }
	}
	for (int i = 0; i < 21; ++i) {
	    try (FileInputStream is = new FileInputStream("./images/" + i + ".png")) {
		viewElements[i] = new Image(is);
	    } catch (IOException e) {
		e.printStackTrace();
	    }
	}
    }
    
    private final void clearView() {
	canvas.getGraphicsContext2D().setFill(Color.LIGHTGRAY);
	canvas.getGraphicsContext2D().clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
    }
    
    public final void draw(Maze maze, Position position) {
	List<Drawable> drawRoutines = gatherDrawRoutines(maze, position);
	clearView();

	// draw backwards
	for (int i = drawRoutines.size() - 1; i >= 0; --i) {
	    drawRoutines.get(i).draw();
	}
    }
    
    public final void drawMonster(Maze maze, int distance) {
	canvas.getGraphicsContext2D().drawImage(tRex[distance], 0, 0);
    }

    public final void drawScreenFailure() {
	clearView();
	canvas.getGraphicsContext2D().drawImage(screenFailure, 0, 0);
    }
    
    public final void drawScreenSuccess() {
	clearView();
	for (int j = 1; j < 24; ++j) {
	    for (int i = 1; i < 23; ++i) {
		if ((i + j) % 2 == 0) {
		    canvas.getGraphicsContext2D().drawImage(viewElements[18], j * ELEMENT_SIZE, i * ELEMENT_SIZE);
		} else {
		    canvas.getGraphicsContext2D().drawImage(viewElements[0], j * ELEMENT_SIZE, i * ELEMENT_SIZE);
		}
	    }
	}
    }

    private final List<Drawable> gatherDrawRoutines(Maze maze, Position position) {
	List<Drawable> routines = new ArrayList<Drawable>();
	for (int i = 0; i < DRAW_DISTANCE; ++i) {
	    if (maze.getTileAtPosition(position.getRelativePosition(i + 1)).equals(Tile.WALL)) {
		routines.add(drawFrontWall[i]);
	    } else {
		if (maze.getExitPosition().equals(position.getRelativePosition(i + 1))) {
		    routines.add(drawExit[i]);
		}
	    }
	}
	for (int i = 0; i < DRAW_DISTANCE; ++i) {
	    if (maze.getTileAtPosition(position.getRelativePosition(i, Side.LEFT)).equals(Tile.WALL)) {
		routines.add(drawLeftWall[i]);
	    } else {
		routines.add(drawLeftPassage[i]);
	    }
	}
	for (int i = 0; i < DRAW_DISTANCE; ++i) {
	    if (maze.getTileAtPosition(position.getRelativePosition(i, Side.RIGHT)).equals(Tile.WALL)) {
		routines.add(drawRightWall[i]);
	    } else {
		routines.add(drawRightPassage[i]);
	    }
	}
	return routines;
    }

    private final void initDrawRoutines() {
	drawExit[0] = () -> {
	    for (int j = 5; j < 20; ++j) {
		for (int i = 5; i < 19; ++i) {
		    if ((i + j) % 2 == 0) {
			canvas.getGraphicsContext2D().drawImage(viewElements[18], j * ELEMENT_SIZE, i * ELEMENT_SIZE);
		    } else {
			canvas.getGraphicsContext2D().drawImage(viewElements[0], j * ELEMENT_SIZE, i * ELEMENT_SIZE);
		    }
		}
	    }
	};
	drawExit[1] = () -> {
	    for (int j = 8; j < 17; ++j) {
		for (int i = 8; i < 16; ++i) {
		    canvas.getGraphicsContext2D().drawImage(viewElements[16], j * ELEMENT_SIZE, i * ELEMENT_SIZE);
		}
	    }
	};
	drawExit[2] = () -> {
	    for (int j = 10; j < 15; ++j) {
		for (int i = 10; i < 14; ++i) {
		    canvas.getGraphicsContext2D().drawImage(viewElements[16], j * ELEMENT_SIZE, i * ELEMENT_SIZE);
		}
	    }
	};
	drawExit[3] = () -> {
	    drawFrontWall[4].draw();
	};
	drawExit[4] = () -> {
	    drawFrontWall[5].draw();
	};
	drawExit[5] = () -> {
	    drawFrontWall[5].draw();
	};
	drawFrontWall[0] = () -> {
	    for (int j = 1; j < 24; ++j) {
		for (int i = 1; i < 23; ++i) {
		    canvas.getGraphicsContext2D().drawImage(viewElements[8], j * ELEMENT_SIZE, i * ELEMENT_SIZE);
		}
	    }
	};
	drawFrontWall[1] = () -> {
	    for (int j = 5; j < 20; ++j) {
		for (int i = 5; i < 19; ++i) {
		    canvas.getGraphicsContext2D().drawImage(viewElements[8], j * ELEMENT_SIZE, i * ELEMENT_SIZE);
		}
	    }
	};
	drawFrontWall[2] = () -> {
	    for (int j = 8; j < 17; ++j) {
		for (int i = 8; i < 16; ++i) {
		    canvas.getGraphicsContext2D().drawImage(viewElements[8], j * ELEMENT_SIZE, i * ELEMENT_SIZE);
		}
	    }
	};
	drawFrontWall[3] = () -> {
	    for (int j = 10; j < 15; ++j) {
		for (int i = 10; i < 14; ++i) {
		    canvas.getGraphicsContext2D().drawImage(viewElements[8], j * ELEMENT_SIZE, i * ELEMENT_SIZE);
		}
	    }
	};
	drawFrontWall[4] = () -> {
	    for (int j = 11; j < 14; ++j) {
		for (int i = 11; i < 13; ++i) {
		    canvas.getGraphicsContext2D().drawImage(viewElements[8], j * ELEMENT_SIZE, i * ELEMENT_SIZE);
		}
	    }
	};
	drawFrontWall[5] = () -> {
	    canvas.getGraphicsContext2D().drawImage(viewElements[9], 12 * ELEMENT_SIZE, 11 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[10], 12 * ELEMENT_SIZE, 12 * ELEMENT_SIZE);
	};
	drawRightWall[0] = () -> {
	    canvas.getGraphicsContext2D().drawImage(viewElements[11], 24 * ELEMENT_SIZE, 0);
	    canvas.getGraphicsContext2D().drawImage(viewElements[14], 24 * ELEMENT_SIZE, 23 * ELEMENT_SIZE);
	    for (int i = 1; i < 23; ++i) {
		canvas.getGraphicsContext2D().drawImage(viewElements[18], 24 * ELEMENT_SIZE, i * ELEMENT_SIZE);
	    }
	};
	drawRightWall[1] = () -> {
	    canvas.getGraphicsContext2D().drawImage(viewElements[11], 20 * ELEMENT_SIZE, 4 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[11], 21 * ELEMENT_SIZE, 3 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[11], 22 * ELEMENT_SIZE, 2 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[11], 23 * ELEMENT_SIZE, ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[14], 20 * ELEMENT_SIZE, 19 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[14], 21 * ELEMENT_SIZE, 20 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[14], 22 * ELEMENT_SIZE, 21 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[14], 23 * ELEMENT_SIZE, 22 * ELEMENT_SIZE);
	    for (int i = 5; i < 19; ++i) {
		canvas.getGraphicsContext2D().drawImage(viewElements[18], 20 * ELEMENT_SIZE, i * ELEMENT_SIZE);
	    }
	    for (int i = 4; i < 20; ++i) {
		canvas.getGraphicsContext2D().drawImage(viewElements[18], 21 * ELEMENT_SIZE, i * ELEMENT_SIZE);
	    }
	    for (int i = 3; i < 21; ++i) {
		canvas.getGraphicsContext2D().drawImage(viewElements[18], 22 * ELEMENT_SIZE, i * ELEMENT_SIZE);
	    }
	    for (int i = 2; i < 22; ++i) {
		canvas.getGraphicsContext2D().drawImage(viewElements[18], 23 * ELEMENT_SIZE, i * ELEMENT_SIZE);
	    }
	};
	drawRightWall[2] = () -> {
	    canvas.getGraphicsContext2D().drawImage(viewElements[11], 17 * ELEMENT_SIZE, 7 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[11], 18 * ELEMENT_SIZE, 6 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[11], 19 * ELEMENT_SIZE, 5 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[14], 17 * ELEMENT_SIZE, 16 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[14], 18 * ELEMENT_SIZE, 17 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[14], 19 * ELEMENT_SIZE, 18 * ELEMENT_SIZE);
	    for (int i = 8; i < 16; ++i) {
		canvas.getGraphicsContext2D().drawImage(viewElements[18], 17 * ELEMENT_SIZE, i * ELEMENT_SIZE);
	    }
	    for (int i = 7; i < 17; ++i) {
		canvas.getGraphicsContext2D().drawImage(viewElements[18], 18 * ELEMENT_SIZE, i * ELEMENT_SIZE);
	    }
	    for (int i = 6; i < 18; ++i) {
		canvas.getGraphicsContext2D().drawImage(viewElements[18], 19 * ELEMENT_SIZE, i * ELEMENT_SIZE);
	    }
	};
	drawRightWall[3] = () -> {
	    canvas.getGraphicsContext2D().drawImage(viewElements[11], 15 * ELEMENT_SIZE, 9 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[11], 16 * ELEMENT_SIZE, 8 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[14], 15 * ELEMENT_SIZE, 14 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[14], 16 * ELEMENT_SIZE, 15 * ELEMENT_SIZE);
	    for (int i = 10; i < 14; ++i) {
		canvas.getGraphicsContext2D().drawImage(viewElements[18], 15 * ELEMENT_SIZE, i * ELEMENT_SIZE);
	    }
	    for (int i = 9; i < 15; ++i) {
		canvas.getGraphicsContext2D().drawImage(viewElements[18], 16 * ELEMENT_SIZE, i * ELEMENT_SIZE);
	    }
	};
	drawRightWall[4] = () -> {
	    canvas.getGraphicsContext2D().drawImage(viewElements[11], 14 * ELEMENT_SIZE, 10 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[14], 14 * ELEMENT_SIZE, 13 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[18], 14 * ELEMENT_SIZE, 11 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[18], 14 * ELEMENT_SIZE, 12 * ELEMENT_SIZE);
	};
	drawRightWall[5] = () -> {
	    canvas.getGraphicsContext2D().drawImage(viewElements[11], 13 * ELEMENT_SIZE, 11 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[14], 13 * ELEMENT_SIZE, 12 * ELEMENT_SIZE);
	};
	drawRightPassage[0] = () -> {
	    for (int i = 1; i < 23; ++i) {
		canvas.getGraphicsContext2D().drawImage(viewElements[8], 24 * ELEMENT_SIZE, i * ELEMENT_SIZE);
	    }
	};
	drawRightPassage[1] = () -> {
	    for (int j = 20; j < 24; ++j) {
		for (int i = 5; i < 19; ++i) {
		    canvas.getGraphicsContext2D().drawImage(viewElements[8], j * ELEMENT_SIZE, i * ELEMENT_SIZE);
		}
	    }
	};
	drawRightPassage[2] = () -> {
	    for (int j = 17; j < 20; ++j) {
		for (int i = 8; i < 16; ++i) {
		    canvas.getGraphicsContext2D().drawImage(viewElements[8], j * ELEMENT_SIZE, i * ELEMENT_SIZE);
		}
	    }
	};
	drawRightPassage[3] = () -> {
	    for (int j = 15; j < 17; ++j) {
		for (int i = 10; i < 14; ++i) {
		    canvas.getGraphicsContext2D().drawImage(viewElements[8], j * ELEMENT_SIZE, i * ELEMENT_SIZE);
		}
	    }
	};
	drawRightPassage[4] = () -> {
	    canvas.getGraphicsContext2D().drawImage(viewElements[8], 14 * ELEMENT_SIZE, 11 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[8], 14 * ELEMENT_SIZE, 12 * ELEMENT_SIZE);
	};
	drawRightPassage[5] = () -> {
	    canvas.getGraphicsContext2D().drawImage(viewElements[9], 13 * ELEMENT_SIZE, 11 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[10], 13 * ELEMENT_SIZE, 12 * ELEMENT_SIZE);
	};
	drawLeftWall[0] = () -> {
	    canvas.getGraphicsContext2D().drawImage(viewElements[12], 0, 0);
	    canvas.getGraphicsContext2D().drawImage(viewElements[7], 0, 23 * ELEMENT_SIZE);
	    for (int i = 1; i < 23; ++i) {
		canvas.getGraphicsContext2D().drawImage(viewElements[18], 0, i * ELEMENT_SIZE);
	    }
	};
	drawLeftWall[1] = () -> {
	    canvas.getGraphicsContext2D().drawImage(viewElements[12], 4 * ELEMENT_SIZE, 4 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[12], 3 * ELEMENT_SIZE, 3 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[12], 2 * ELEMENT_SIZE, 2 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[12], ELEMENT_SIZE, ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[7], 4 * ELEMENT_SIZE, 19 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[7], 3 * ELEMENT_SIZE, 20 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[7], 2 * ELEMENT_SIZE, 21 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[7], ELEMENT_SIZE, 22 * ELEMENT_SIZE);
	    for (int i = 5; i < 19; ++i) {
		canvas.getGraphicsContext2D().drawImage(viewElements[18], 4 * ELEMENT_SIZE, i * ELEMENT_SIZE);
	    }
	    for (int i = 4; i < 20; ++i) {
		canvas.getGraphicsContext2D().drawImage(viewElements[18], 3 * ELEMENT_SIZE, i * ELEMENT_SIZE);
	    }
	    for (int i = 3; i < 21; ++i) {
		canvas.getGraphicsContext2D().drawImage(viewElements[18], 2 * ELEMENT_SIZE, i * ELEMENT_SIZE);
	    }
	    for (int i = 2; i < 22; ++i) {
		canvas.getGraphicsContext2D().drawImage(viewElements[18], ELEMENT_SIZE, i * ELEMENT_SIZE);
	    }
	};
	drawLeftWall[2] = () -> {
	    canvas.getGraphicsContext2D().drawImage(viewElements[12], 7 * ELEMENT_SIZE, 7 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[12], 6 * ELEMENT_SIZE, 6 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[12], 5 * ELEMENT_SIZE, 5 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[7], 7 * ELEMENT_SIZE, 16 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[7], 6 * ELEMENT_SIZE, 17 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[7], 5 * ELEMENT_SIZE, 18 * ELEMENT_SIZE);
	    for (int i = 8; i < 16; ++i) {
		canvas.getGraphicsContext2D().drawImage(viewElements[18], 7 * ELEMENT_SIZE, i * ELEMENT_SIZE);
	    }
	    for (int i = 7; i < 17; ++i) {
		canvas.getGraphicsContext2D().drawImage(viewElements[18], 6 * ELEMENT_SIZE, i * ELEMENT_SIZE);
	    }
	    for (int i = 6; i < 18; ++i) {
		canvas.getGraphicsContext2D().drawImage(viewElements[18], 5 * ELEMENT_SIZE, i * ELEMENT_SIZE);
	    }
	};
	drawLeftWall[3] = () -> {
	    canvas.getGraphicsContext2D().drawImage(viewElements[12], 9 * ELEMENT_SIZE, 9 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[12], 8 * ELEMENT_SIZE, 8 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[7], 9 * ELEMENT_SIZE, 14 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[7], 8 * ELEMENT_SIZE, 15 * ELEMENT_SIZE);
	    for (int i = 10; i < 14; ++i) {
		canvas.getGraphicsContext2D().drawImage(viewElements[18], 9 * ELEMENT_SIZE, i * ELEMENT_SIZE);
	    }
	    for (int i = 9; i < 15; ++i) {
		canvas.getGraphicsContext2D().drawImage(viewElements[18], 8 * ELEMENT_SIZE, i * ELEMENT_SIZE);
	    }
	};
	drawLeftWall[4] = () -> {
	    canvas.getGraphicsContext2D().drawImage(viewElements[12], 10 * ELEMENT_SIZE, 10 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[7], 10 * ELEMENT_SIZE, 13 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[18], 10 * ELEMENT_SIZE, 11 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[18], 10 * ELEMENT_SIZE, 12 * ELEMENT_SIZE);
	};
	drawLeftWall[5] = () -> {
	    canvas.getGraphicsContext2D().drawImage(viewElements[12], 11 * ELEMENT_SIZE, 11 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[7], 11 * ELEMENT_SIZE, 12 * ELEMENT_SIZE);
	};
	drawLeftPassage[0] = () -> {
	    for (int i = 1; i < 23; ++i) {
		canvas.getGraphicsContext2D().drawImage(viewElements[8], 0, i * ELEMENT_SIZE);
	    }
	};
	drawLeftPassage[1] = () -> {
	    for (int j = 1; j < 5; ++j) {
		for (int i = 5; i < 19; ++i) {
		    canvas.getGraphicsContext2D().drawImage(viewElements[8], j * ELEMENT_SIZE, i * ELEMENT_SIZE);
		}
	    }
	};
	drawLeftPassage[2] = () -> {
	    for (int j = 5; j < 8; ++j) {
		for (int i = 8; i < 16; ++i) {
		    canvas.getGraphicsContext2D().drawImage(viewElements[8], j * ELEMENT_SIZE, i * ELEMENT_SIZE);
		}
	    }
	};
	drawLeftPassage[3] = () -> {
	    for (int j = 8; j < 10; ++j) {
		for (int i = 10; i < 14; ++i) {
		    canvas.getGraphicsContext2D().drawImage(viewElements[8], j * ELEMENT_SIZE, i * ELEMENT_SIZE);
		}
	    }
	};
	drawLeftPassage[4] = () -> {
	    canvas.getGraphicsContext2D().drawImage(viewElements[8], 10 * ELEMENT_SIZE, 11 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[8], 10 * ELEMENT_SIZE, 12 * ELEMENT_SIZE);
	};
	drawLeftPassage[5] = () -> {
	    canvas.getGraphicsContext2D().drawImage(viewElements[9], 11 * ELEMENT_SIZE, 11 * ELEMENT_SIZE);
	    canvas.getGraphicsContext2D().drawImage(viewElements[10], 11 * ELEMENT_SIZE, 12 * ELEMENT_SIZE);
	};
    }
}
