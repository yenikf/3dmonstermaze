/*
 * @(#)Main.java	1.0 19/12/18 Jan Feific
 * 
 * Copyright (c) 2019 Jan Feific. All Rights Reserved.
 */

package monstermaze;

import monstermaze.graphics.AppFrame;

/**
 * Default start up class.
 * For some more technical info this program was based on, check
 * "https://softtangouk.wixsite.com/soft-tango-uk/3d-monster-maze".
 */
public final class Main {
    /**
     * Create new application window and run the program.
     * @param sysargs, not used
     */
    public static final void main(String[] args) {
	
	AppFrame view = new AppFrame();
	view.runApplication();

	return;
    }
}
