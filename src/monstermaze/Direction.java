/*
 * @(#)Direction.java	1.0 19/12/18 Jan Feific
 * 
 * Copyright (c) 2019 Jan Feific. All Rights Reserved.
 */

package monstermaze;

import java.util.Random;

/**
 * An enum representing the four cardinal directions.
 */
public enum Direction {
    NORTH, EAST, SOUTH, WEST;

    /**
     * Pick a random direction.
     * @return random direction
     */
    public static final Direction random() {
	Random generator = new Random();
	int random = generator.nextInt(4);
	if (random == 0) {
	    return NORTH;
	} else if (random == 1) {
	    return EAST;
	} else if (random == 2) {
	    return SOUTH;
	} else {
	    return WEST;
	}
    }

    /**
     * Turns 90 degrees to the left.
     * @return New Direction object with changed state.
     */
    public final Direction turnLeft() {
	if (this == NORTH) {
	    return WEST;
	} else if (this == EAST) {
	    return NORTH;
	} else if (this == SOUTH) {
	    return EAST;
	} else {
	    return SOUTH;
	}
    }

    /**
     * Turns 90 degrees to the right.
     * @return New Direction object with changed state.
     */
    public final Direction turnRight() {
	if (this == NORTH) {
	    return EAST;
	} else if (this == EAST) {
	    return SOUTH;
	} else if (this == SOUTH) {
	    return WEST;
	} else {
	    return NORTH;
	}
    }
}
