/*
 * @(#)Position.java	1.0 19/12/18 Jan Feific
 * 
 * Copyright (c) 2019 Jan Feific. All Rights Reserved.
 */

package monstermaze;

/**
 * Class representing a particular position. Position means exact place
 * on a 2d grid (x and y coordinates) and a direction of view. 
 */
public final class Position {
    private Direction direction;
    private int xCoordinate;
    private int yCoordinate;
    
    /**
     * Default constructor. Player class uses this position as a base.
     * @see Player
     */
    public Position() {
	this(0, 0, Direction.SOUTH);
    }
    
    /**
     * Position at exact spot.
     * @param x The x coordinate.
     * @param y The y coordinate.
     * @param dir The direction of view.
     */
    public Position(int x, int y, Direction dir) {
	xCoordinate = x;
	yCoordinate = y;
	direction = dir;
    }
    
    /**
     * Causes the position to move one square forward, whichever direction it
     * may be.
     */
    public final void advance() {
	if (direction == Direction.NORTH) {
	    --yCoordinate;
	} else if (direction == Direction.EAST) {
	    ++xCoordinate;
	} else if (direction == Direction.SOUTH) {
	    ++yCoordinate;
	} else {
	    --xCoordinate;
	}
    }
    
    /**
     * Compares two Position object for equivalence. Equivalent positions have
     * both the same x and y coordinates but direction may differ.
     */
    @Override
    public final boolean equals(Object otherPosition) {
	if (otherPosition instanceof Position) {
	    Position p = (Position) otherPosition;
	    return (xCoordinate == p.xCoordinate) && (yCoordinate == p.yCoordinate);
	}
	return false;
    }

    /**
     * Query the object for its Direction property.
     * @return The direction this object heads to.
     */
    public final Direction getDirection() {
	return direction;
    }
    
    /**
     * Returns position which has coordinates relative to the original position.
     * The new position is placed in direct line in front of the original one
     * (respecting the direction of the original position) at the specified
     * distance.
     * @param distance The distance from the original position.
     * @return New Position object at the specified distance.
     * @see #getRelativePosition(int, Side)
     */
    public final Position getRelativePosition(int distance) {
	if (direction == Direction.NORTH) {
	    return new Position(getX(), getY() - distance, getDirection());
	} else if (direction == Direction.EAST) {
	    return new Position(getX() + distance, getY(), getDirection());
	} else if (direction == Direction.SOUTH) {
	    return new Position(getX(), getY() + distance, getDirection());
	} else {
	    return new Position(getX() - distance, getY(), getDirection());
	}
    }
    
    /**
     * Overloaded variant with the capability of skewing the view one step
     * to the left/right side.
     * @param distance The distance from the original position.
     * @param side The Side enum representing the one step left/right we look.
     * @return New Position object at the specified distance.
     * @see #getRelativePosition(int)
     * @see Side
     */
    public final Position getRelativePosition(int distance, Side side) {
	if (direction == Direction.NORTH) {
	    if (side == Side.LEFT) {
		return new Position(getX() - 1, getY() - distance, getDirection());
	    } else {
		return new Position(getX() + 1, getY() - distance, getDirection());
	    }
	} else if (direction == Direction.EAST) {
	    if (side == Side.LEFT) {
		return new Position(getX() + distance, getY() - 1, getDirection());
	    } else {
		return new Position(getX() + distance, getY() + 1, getDirection());
	    }
	} else if (direction == Direction.SOUTH) {
	    if (side == Side.LEFT) {
		return new Position(getX() + 1, getY() + distance, getDirection());
	    } else {
		return new Position(getX() - 1, getY() + distance, getDirection());
	    }
	} else {
	    if (side == Side.LEFT) {
		return new Position(getX() - distance, getY() + 1, getDirection());
	    } else {
		return new Position(getX() - distance, getY() - 1, getDirection());
	    }
	}
    }

    /**
     * Query for x coordinate.
     * @return The x coordinate of the position.
     */
    public final int getX() {
	return xCoordinate;
    }
    
    /**
     * Query for y coordinate.
     * @return The y coordinate of the position.
     */
    public final int getY() {
	return yCoordinate;
    }
    
    /**
     * Checks whether the y coordinate fits into a specific interval.
     * @param low The low-end of the asked interval, inclusive.
     * @param high The high-end of the asked interval, inclusive.
     * @return True if the y coordinate fits.
     */
    public final boolean isXInRange(int low, int high) {
	if (low <= xCoordinate && xCoordinate <= high) {
	    return true;
	}
	return false;
    }
    
    /**
     * Checks whether the x coordinate fits into a specific interval.
     * @param low The low-end of the asked interval, inclusive.
     * @param high The high-end of the asked interval, inclusive.
     * @return True if the x coordinate fits.
     */
    public final boolean isYInRange(int low, int high) {
	if (low <= yCoordinate && yCoordinate <= high) {
	    return true;
	}
	return false;
    }

    /**
     * Changes the direction of the object.
     * @param dir New direction.
     */
    public final void setDirection(Direction dir) {
	direction = dir;
    }
    
    /**
     * Changes the direction of an object so that it effectively turns left.
     */
    public final void turnLeft() {
	direction = direction.turnLeft();
    }
    
    /**
     * Changes the direction of an object so that it effectively turns right.
     */
    public final void turnRight() {
	direction = direction.turnRight();
    }
}
