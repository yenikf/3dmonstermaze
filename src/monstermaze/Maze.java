/*
 * @(#)Maze.java	1.0 19/12/18 Jan Feific
 * 
 * Copyright (c) 2019 Jan Feific. All Rights Reserved.
 */

package monstermaze;

import java.util.Random;

/**
 * Class Maze represents a maze, a grid of walls and passages.
 */
public final class Maze {
    private static final int MAZE_WIDTH = 16;
    private static final int MAZE_HEIGHT = 18;
    private Tile maze[][];
    private Position exitPosition = null;
    
    /**
     * Default constructor. Creates a random maze with set dimensions.
     * @see generateNew
     * @see placeExit
     */
    public Maze() {
	maze = new Tile[MAZE_WIDTH][MAZE_HEIGHT];
	for (int y = 0; y < MAZE_HEIGHT; ++y) {
	    for (int x = 0; x < MAZE_WIDTH; ++x) {
		maze[x][y] = Tile.WALL;
	    }
	}
	generateNew();
	placeExit();
    }
    
    /**
     * Generates new maze. Adapts the algorithm described at
     * "https://softtangouk.wixsite.com/soft-tango-uk/3d-monster-maze".
     * Starts with solid space it then tunnels passages of random length
     * in random direction while avoiding the 2x2 square pattern.
     */
    private final void generateNew() {
	Random lengthGenerator = new Random();
	maze[0][0] = Tile.EMPTY;
	Position position = new Position(0, 0, Direction.SOUTH);
	int tilesToGenerate = 800;
	while (tilesToGenerate > 0) {
	    position.setDirection(Direction.random());
	    int passageLength = lengthGenerator.nextInt(5) + 1;
	    for (int i = 0; i < passageLength; ++i) {
		Position nextPosition = position.getRelativePosition(1);
		if (fitsInMaze(nextPosition)) {

		    // we have to avoid the 2x2 square pattern

		    if (getTileAtPosition(nextPosition.getRelativePosition(0, Side.LEFT)).equals(Tile.EMPTY)) {
			if (getTileAtPosition(nextPosition.getRelativePosition(-1, Side.LEFT)).equals(Tile.EMPTY)) {
			    break;
			}
			if (getTileAtPosition(nextPosition.getRelativePosition(1, Side.LEFT)).equals(Tile.EMPTY)
				&& getTileAtPosition(nextPosition.getRelativePosition(1)).equals(Tile.EMPTY)) {
			    break;
			}
		    }
		    if (getTileAtPosition(nextPosition.getRelativePosition(0, Side.RIGHT)).equals(Tile.EMPTY)) {
			if (getTileAtPosition(nextPosition.getRelativePosition(-1, Side.RIGHT)).equals(Tile.EMPTY)) {
			    break;
			}
			if (getTileAtPosition(nextPosition.getRelativePosition(1, Side.RIGHT)).equals(Tile.EMPTY)
				&& getTileAtPosition(nextPosition.getRelativePosition(1)).equals(Tile.EMPTY)) {
			    break;
			}
		    }
		    position = nextPosition;
		    maze[position.getX()][position.getY()] = Tile.EMPTY;
		    --tilesToGenerate;
		} else {
		    break;
		}
	    }
	}
    }
    
    /**
     * Places an exit square into the maze. It finds an empty tile within last
     * seven rows and surrounds it from any side but one with a wall tile.
     */
    private final void placeExit() {
	Random coordinate = new Random();
	do {
	    int exitCoordinateX = coordinate.nextInt(getMazeWidth());
	    int exitCoordinateY = getMazeHeight() - coordinate.nextInt(7) - 1;
	    exitPosition = new Position(exitCoordinateX, exitCoordinateY, Direction.SOUTH);
	} while (getTileAtPosition(exitPosition).equals(Tile.WALL));
	boolean hasPath = false;

	// neighbouring empty tile means existing path to the exit
	while (hasPath != true) {
	    if (getTileAtPosition(exitPosition.getRelativePosition(1)).equals(Tile.EMPTY)) {
		hasPath = true;
	    }
	    exitPosition.turnLeft();
	}
	
	// let's close all other neighbouring empty tiles so there's only one path to the exit
	while (!exitPosition.getDirection().equals(Direction.SOUTH)) {
	    if (getTileAtPosition(exitPosition.getRelativePosition(1)).equals(Tile.EMPTY)) {
		maze[exitPosition.getRelativePosition(1).getX()][exitPosition.getRelativePosition(1).getY()] = Tile.WALL;
	    }
	    exitPosition.turnLeft();
	}
	System.out.format("Exit spawned at x: %d, y: %d.\n", exitPosition.getX(), exitPosition.getY());
    }
    
    /**
     * Checks if particular the position (its x and y coordinates) fits into the
     * maze dimensions.
     * @param position The position we want to check (ignoring the direction attribute).
     * @return True if the position is in the maze.
     * @see Position
     */
    public final boolean fitsInMaze(Position position) {
	if (position.isXInRange(0, MAZE_WIDTH - 1) && position.isYInRange(0, MAZE_HEIGHT - 1)) {
	    return true;
	}
	return false;
    }
    
    /**
     * Returns position of the exit.
     * @return Position object with coordinates of the maze exit.
     * @see Position
     */
    public final Position getExitPosition() {
	return exitPosition;
    }
    
    /**
     * Returns the height of the maze.
     * @return The height of the maze.
     */
    public final int getMazeHeight() {
	return MAZE_HEIGHT;
    }
    
    /**
     * Returns the width of the maze.
     * @return The width of the maze.
     */
    public final int getMazeWidth() {
	return MAZE_WIDTH;
    }
    
   /**
    * Returns a maze tile referenced by the Position object coordinates.
    * @param position Position (its coordinates) we querry for tile.
    * @return The tile referenced by the Position object coordinates (WALL or EMPTY).
    * @see Tile
    */
    public final Tile getTileAtPosition(Position position) {
	if (fitsInMaze(position)) {
	    return maze[position.getX()][position.getY()];
	} else {
	    return Tile.WALL;
	}
    }
    
    /**
     * Prints the maze layout to the console output.
     */
    @Override
    public String toString() {
	StringBuilder string = new StringBuilder();
	for (int y = 0; y < MAZE_HEIGHT; ++y) {
	    for (int x = 0; x < MAZE_WIDTH; ++x) {
		if (x == exitPosition.getX() && y == exitPosition.getY()) {
		    string.append("E");
		} else {
		    string.append(maze[x][y]);
		}
	    }
	    string.append("\n");
	}
	return string.toString();
    }
}
