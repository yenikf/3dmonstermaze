/*
 * @(#)Player.java	1.0 19/12/18 Jan Feific
 * 
 * Copyright (c) 2019 Jan Feific. All Rights Reserved.
 */

package monstermaze;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 * Class representing the player entity.
 * Its main property is its position in the maze, but it also checks for
 * collisions and handles the keyboard input.
 */
public final class Player {
    private int scoreCount = 0;
    private Maze maze = null;
    private Position position = null;
    
    /**
     * Default constructor. Places the player at the default coordinates (0,0)
     * of the particular maze.
     * @param m the maze the player is held in
     * @see Maze
     * @see Position
     */
    public Player(Maze m) {
	position = new Position();
	maze = m;
    }
    
    /**
     * Makes one step forward in any direction the player is looking, while
     * checking for collision with walls. Increases score by five points each step.
     */
    public final void advance() {
	if (maze.getTileAtPosition(position.getRelativePosition(1)).equals(Tile.EMPTY)) {
	    scoreCount += 5;
	    position.advance();
	}
    }
    
    /**
     * Gets the position of the Player object.
     * @return The position of the player.
     */
    public final Position getPosition() {
	return position;
    }
    
    /**
     * Returns the player score.
     * @return The score count.
     */
    public final int getScore() {
	return scoreCount;
    }
    
    /**
     * Handles the keyboard input. There are only four keys that need to be
     * taken care of: LEFT and RIGHT arrows for turning, UP arrow for going
     * one step forward, and ESCAPE for terminating the program.
     */
    public final EventHandler<KeyEvent> handleInput = (KeyEvent event) -> {
	if (event.getCode() == KeyCode.LEFT) {
	    this.turnLeft();
	} else if (event.getCode() == KeyCode.RIGHT) {
	    this.turnRight();
	} else if (event.getCode() == KeyCode.UP) {
	    this.advance();
	} else if (event.getCode() == KeyCode.ESCAPE) {
	    Platform.exit();
	}
    };

    /**
     * Causes the player to turn 90 degrees left.
     */
    public final void turnLeft() {
	position.turnLeft();
    }
    
    /**
     * Causes the player to turn 90 degrees right.
     */
    public final void turnRight() {
	position.turnRight();
    }
}
