/*
 * @(#)Side.java	1.0 19/12/18 Jan Feific
 * 
 * Copyright (c) 2019 Jan Feific. All Rights Reserved.
 */

package monstermaze;

/**
 * Simple enum representing the left or right hand side of view.
 * Its advantage is that it is indifferent to the four cardinal directions.
 */
public enum Side {
    LEFT, RIGHT;
}
